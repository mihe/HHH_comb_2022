#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_6XS_cookWS_FullSysObs.root r_tH Obs
#python printCorrelation.py /publicfs/atlas/atlasnew/higgs/hgg/lugc/workspaceCombiner_2021/hcombcouplingsummer2021_glu/workspaces/v0/combination/WS-Comb-mu_uncond_fit.root mu Obs
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p ATLAS_PRW_DATASF_Rel21 -d Exp
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p ATLAS_LUMI_RUN2_CORR -d Exp
python3 printCorrelation.py -f /scratchfs/atlas/hemx/HCOMB_v2/HHH_comb_2022/workspaces/v0/best_fit/obs/WS-singleH-k3only_migrad_1E-05.root -p k3 -d Obs -s 1
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p r_VBF -d Exp -s 1
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p r_WH -d Exp -s 1
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p r_ZH -d Exp -s 1
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p r_ttH -d Exp -s 1
#python printCorrelation.py -f /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root -p r_ggF -d Exp -s 1
#python printCorrelation.py /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root r_VBF Exp
#python printCorrelation.py /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root r_WH Exp
#python printCorrelation.py /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root r_ZH Exp
#python printCorrelation.py /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_asimov_cookWS_FullSys.root r_ttH Exp
#for channel in WS-VHWW-mu_lumi_split
##for channel in WS-HZZ-mu_lumi_split WS-HGam-mu WS-HWW-mu_lumi_split WS-VHbb-mu_lumi_split WS-VHcc-mu_lumi_split WS-ttH-MLnotau-mu_lumi_split WS-ttH-MLtau-mu_lumi_split WS-ttHbb-mu WS-Htautau-mu_lumi_split WS-VBFbb-mu_lumi_split_FullRun2 WS-HZy-mu WS-Hmumu-mu_140ifb WS-VHWW-mu_lumi_split
#do
#python printCorrelation.py /afs/ihep.ac.cn/users/r/rankl/atlasnew/combination/workspaceCombiner/hcombcouplingsummer2021/workspaces/v0/reparam_input/${channel}_data_hessefit.root mu Obs
#done
#python printCorrelation.py /publicfs/atlas/atlasnew/higgs/hgg/lugc/workspaceCombiner_2021/hcombcouplingsummer2021_glu/workspaces/v0/combination/WS-Comb-mu_asimov_uncond_fit.root mu Exp
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/reparam_input/WS-HWW-5mu_validate_data_statFit.root mu_VBF Obs
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/parameterization/WS-Comb-CWZtobcta_BR_BSM_0_unconditional.root Cmu Obs
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/combination/WS-Comb-mu_uncond_fit.root mu Obs
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/combination/WS-Comb-mu_asimov_uncond_fit.root mu Exp
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_6XS_cookWS_FullSys.root r_tH Exp
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_cookWS_FullSys.root r_ZH Exp
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_cookWS.root r_WH Obs
#python printCorrelation.py hcombcouplingsummer2021/workspaces/v0/parameterization/fit_nll_5XS_cookWS.root r_ZH Obs
