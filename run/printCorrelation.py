import ROOT
#import collections
import sys

import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-f","--filePath",type=str,default="None")
parser.add_argument("-p","--pOI",type=str,default="mu")
parser.add_argument("-d","--data",type=str,default="Obs")
parser.add_argument("-s","--skim",type=int,default=0)
args=parser.parse_args()

wS=ROOT.TFile(args.filePath)
fileName=args.filePath.split("/")[-1].split(".")[0]
#wS=ROOT.TFile("/afs/desy.de/user/r/rankunli/nfs/combination/workspaceCombiner/hcombcouplingsummer2020/workspaces/v3/combination/WS-Comb-mu_140ifb_uncond_fit.root")
#fitResult=wS.Get("fitresult_minimizer_combData")
fitResult=wS.Get("fitResult")

floatPar=fitResult.floatParsFinal()

parNameList=[]
correlationAbsList=[]
correlationList=[]
centralList=[]
errList=[]
#correlationAbsDic={}
#correlationDic={}

pOIName=args.pOI
for iPar in range(floatPar.getSize()):
    rooPar=floatPar.at(iPar)
    parName=rooPar.GetName()

    #if parName==pOIName or "unconstr_" in parName or "gamma_" in parName or parName.startswith("r_"):
    #    continue

#    nPName=parName.replace("mu_","").replace("ATLAS_","").replace("TheorySig_","").replace("BkgTheory_","").replace("unconstr_","")
    nPName=parName
    correlation=fitResult.correlation(pOIName,parName)

    parNameList.append(nPName)
    correlationAbsList.append(abs(correlation))
    correlationList.append(correlation)
    #correlationAbsDic[nPName]=abs(correlation)
    #correlationDic[nPName]=correlation

    central=rooPar.getVal()
    err=rooPar.getError()

    centralList.append(central)
    errList.append(err)

import pandas as pd

nPDF=pd.DataFrame(data={"Name":parNameList,"Correlation":correlationList,"Central":centralList,"Error":errList,"CorrAbs":correlationAbsList},columns=["Name","Correlation","Central","Error","CorrAbs"])
nPDF.sort_values("CorrAbs",ascending=False,inplace=True,ignore_index=True)

dataType=args.data
nPDF.to_csv("correlation%s_%s%s.csv"%(fileName,pOIName,dataType),columns=["Name","Correlation","Central","Error"])

if args.skim==1:
    skimNPDF=nPDF[(nPDF["Name"].str.contains("ATLAS_"))|(nPDF["Name"].str.contains("TheorySig_"))|(nPDF["Name"].str.contains("BkgTheory_"))]
    skimNPDF=skimNPDF[(abs(skimNPDF["Central"])>0.5)|(skimNPDF["Error"]<0.7)]
    skimNPDF.reset_index(inplace=True)

    skimNPDF.to_csv("correlation%s_%s%sPullConstrain.csv"%(fileName,pOIName,dataType),columns=["Name","Correlation","Central","Error"])
#sorted_x = sorted(correlationAbsDic.items(), key=lambda kv: kv[1],reverse=True)
#
#sorted_dict = collections.OrderedDict(sorted_x)
#
#correlatonStr=""
#for iNP in sorted_dict:
#    correlatonStr+="%s, %f\n"%(iNP,correlationDic[iNP])
#
#correlationFile=open("correlation%s_%s%s.csv"%(fileName,pOIName,dataType),"w")
##correlationFile=open("correlationMuObs.csv","w")
#correlationFile.write(correlatonStr)
