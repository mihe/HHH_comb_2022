#!/bin/bash

### HH workspaces
quickAsimov --minTolerance 1e-04 -x HHH_comb_2022/asimov/Asimov_bbyy.xml -w combWS -m ModelConfig -d combData | tee genAsimov_HH_bbyy.log
quickAsimov --minTolerance 5e-05 -x HHH_comb_2022/asimov/Asimov_bbtautau.xml -w combined -m ModelConfig -d obsData | tee genAsimov_HH_bbtautau.log
quickAsimov --minTolerance 5e-05 -x HHH_comb_2022/asimov/Asimov_bbbb.xml -w combined -m ModelConfig -d obsData | tee genAsimov_HH_bbbb.log

## HH combined WS
quickAsimov --minTolerance 5e-05 -x HHH_comb_2022/asimov/Asimov_HHComb.xml -w combWS -m ModelConfig -d combData | tee genAsimov_HHComb.log

## H+HH WS
quickAsimov --minTolerance 5e-05 -x HHH_comb_2022/asimov/Asimov_HHH.xml -w combWS -m ModelConfig -d combData | tee genAsimov_HHH.log
