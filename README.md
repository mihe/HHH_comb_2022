# HHH Comb 2022

The configs used for H+HH combination in 2022

All the input workspaces come from date stamp `20220514` (updated dynamically):
```
/eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/combination/FullRun2Workspaces/original/HHH2022/2022*
```

## How to use the codes
You are recommended to run it on hcomb-docker so that there wouldn't be additional efforts to setup the complex environment.
The latest docker image `analyzer:2-2` is suggested for the study
```
singularity shell /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas_higgs_combination/software/hcomb-docker/analyzer:2-2
```
### clone the codes
```
mkdir -vp workspaceCombiner
cd workspaceCombiner
git clone ssh://git@gitlab.cern.ch:7999/glu/HHH_comb_2022.git
```

### run the modifications and combination
Before the combination, you should prepare the input workspaces well. In this case, it means to perform kappa parameterization correctly on the input workspaces. The input workspaces consist of 1 single-Higgs WS and 3 Di-Higgs WS (`bbbb`, `bbyy` and `bbtautau`).

You can run the modification => combination automaticallly through
```
source HHH_comb_2022/scripts/v0/combineHHH.sh
```
The `combination_HH.xml` is for Di-Higgs only combination, while `combination_HHH.xml` is for single-Higgs + Di-Higgs combination


### produce asimov dataset
All the asimov datasets are derived from fit to obsData in the corresponding WS.

```
source HHH_comb_2022/scripts/v0/genAsimov.sh
```

## The workspaces available
You are strongly recommended to obtain the workspaces from `original_input` by yourself through the scripts and configs in the git repository, even though there have been some ready WS produced by me.
- `reparam_input`: the kappa-parameterized workspaces and the corresponding ones with asimov dataset, which would be used as inputs of combination
- `combination`: the combined workspaces and the corresponding ones with asimov dataset.
- `best_fit`: the unconditional post-fit workspaces and the corresponding ones with asimov dataset, which can be used as baseline WS of NLL scan. Now following models are provided:
	- **klambda-only**(only profiling `klambda` while fixing the other modifiers to SM) 
	- **klambda-kt**(profiling `klambda` and `kt` while fixing the other modifiers to SM). 
	- **generic** (profiling all the POIs in the WS: `klambda`, `kt`, `kV`, `kb`, `ktau`)
Either `_migrad_` or `_hesse_` is ok to be baseline of NLL scan.
