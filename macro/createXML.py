#!/usr/bin/env python

import sys, os, time, getopt, copy, random, math
from glob import glob
if __name__=="__main__" :

    if len(sys.argv)<2:
        print("Usage: python "+sys.argv[0]+" <input xml> <output xml>")
        sys.exit()
    else:
        print(sys.argv)

    inputxml=sys.argv[1]
    outputxml=sys.argv[2]

    fin=open(inputxml,"r")
    fout=open(outputxml,"w")

    skip=False

    print("The following files will be inserted:")
    for line in fin:
        #print line
        if '&' in line:
            insertfilename=line.rstrip().split('&')[1]
            print(insertfilename)
            insertfile=open(insertfilename,"r")
            for insert in insertfile:
                fout.write(insert)
            insertfile.close()
        else:
            fout.write(line)

    fout.close()

    print("File "+outputxml+" saved.")
