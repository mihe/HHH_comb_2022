import glob, os, sys, argparse
import commands
import ROOT
import json
import xml.etree.ElementTree
from collections import *

def getAttribute(e, name):
    for att in e.attrib:
        if att==name: return att
    return None

def findChild(e, name):
    for child in e:
        if child.tag==name: return child
    return None

def alert(message):
    raise Exception("\033[91m "+message+" \033[0m")

def warning(message):
    print ("\033[91m "+message+" \033[0m")
    
if __name__=="__main__" :

    parser=argparse.ArgumentParser()
    parser.add_argument("-x", "--xml", type=str, help="Input XML file path", required=True)
    parser.add_argument("--outputXML", help="Output XML file path", type=str, default="")
    parser.add_argument("--outputJSON", help="Output JSON file path", type=str, default="POI.json")

    args=parser.parse_args()
    
    outputXMLFileName=args.outputXML

    if outputXMLFileName!="": fxmlout=open(outputXMLFileName,"w")
    
    e=xml.etree.ElementTree.parse(args.xml).getroot()
    
    poiListBase=[]
    poiDict=OrderedDict()
    
    for channel in e.findall("Channel"):
        #print channel.tag, channel.attrib
        channelName=channel.get("Name")
        isCombined=bool(channel.get("IsCombined"))
        #print channelName
        poiList=findChild(channel, "ModelPOI").get("Name").replace(" ","").replace("\n","").replace("\t","").split(",")

        if isCombined:      # Scrutinize the input workspace
            poiListBase=poiList
        else:
            # Check whether POI list match: can only do by eye!
            if len(poiList)!=len(poiListBase):
                if len(poiList)>len(poiListBase): alert("POI list in channel "+channelName+" is too long")
                else:
                    for i in range(len(poiListBase)-len(poiList)): poiList.append("dummy")
            channelPOIDict=OrderedDict(zip(poiListBase, poiList))
            for key, value in channelPOIDict.iteritems():
                if value=="dummy": del channelPOIDict[key]
            #print channelPOIDict
            poiDict[channelName]=channelPOIDict
            fileName=findChild(channel, "File").get("Name")
            wsName=findChild(channel, "Workspace").get("Name")
            mcName=findChild(channel, "ModelConfig").get("Name")
            dsName=findChild(channel, "ModelData").get("Name")

            # Check whether the basic objects exist
            fws=ROOT.TFile(fileName)
            if not fws: alert("File "+fileName+" cannot be opened")
            ws=fws.Get(wsName)
            if not ws: alert("Workspace "+wsName+" cannot be found in file "+fileName)
            mc=ws.obj(mcName)
            if not mc: alert("ModelConfig "+mcName+" cannot be found in workspace "+fileName)
            ds=ws.data(dsName)
            if not ds: alert("Dataset "+dsName+" cannot be found in workspace "+fileName)

            # Check whether all the POIs exist
            for POI in poiList:
                if POI=="dummy": continue
                if not ws.var(POI): alert("POI "+POI+" does not exist in workspace "+fileName)

            # Check whether all the NPs are covered
            renameMap=findChild(channel, "RenameMap")
            NPListXML=[]
            for syst in renameMap.findall("Syst"):
                NPName=syst.get("OldName")
                if ',' in NPName: NPName=NPName.split('(')[1].split(',')[0]
                #if not ws.var(NPName): warning("NP "+ NPName + " in XML file does not exist in workspace "+fileName)
                NPListXML.append(NPName)
                
            NPSet=mc.GetNuisanceParameters()
            iter=NPSet.createIterator()
            parg=iter.Next()
            while parg:
                NPName=parg.GetName()
                if NPName not in NPListXML: warning("NP "+ NPName + " in workspace "+fileName+" is not covered by XML file "+args.xml)
                parg=iter.Next()
            fws.Close()
    with open(args.outputJSON,"w") as outjson:
        json.dump(poiDict, outjson, indent=4, separators=(',', ': '))
