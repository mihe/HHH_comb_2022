using namespace RooFit;
using namespace RooStats;

vector<pair<TString, double> > traceBack(RooAbsReal *arg, ModelConfig *mc){
  vector<pair<TString, double> > list;
  TIterator *iter=arg->clientIterator();
  RooAbsReal *client=NULL;
  while((client=dynamic_cast<RooAbsReal*>(iter->Next()))){
    if(dynamic_cast<RooAbsPdf*>(client)){
      if(dynamic_cast<RooRealSumPdf*>(client)){ // HistFactory workspace
	RooRealSumPdf *pdf=dynamic_cast<RooRealSumPdf*>(client);
        int idx=pdf->funcList().index(arg);
	RooAbsReal *binWidth=(RooAbsReal*)pdf->coefList().at(idx);
	// cout<<idx<<endl;
	RooArgSet *obs=(RooArgSet*)(pdf->getObservables(mc->GetObservables()));
	// cout<<obs<<endl;
	// //obs->Print();
	RooAbsReal *integral=arg->createIntegral(*obs);
	// cout<<integral->getVal()<<" "<<binWidth->getVal()<<endl;
	double yield=(integral->getVal()) * (binWidth->getVal());
	// double yield=integral->getVal();
	// double yield=(integral->getVal()) / (binWidth->getVal());
	list.push_back(make_pair(arg->GetName(),yield));
      }
      else{
	list.push_back(make_pair(arg->GetName(),arg->getVal()));
      }
    }
    else{
      vector<pair<TString, double> > temp=traceBack(client, mc);
      list.insert(list.end(), temp.begin(), temp.end());
    }
  }
  return list;
}

double getLuminosity(TString varName){
  if(varName.Contains("channel_HGam")||varName.Contains("channel_HZZ")) return 79.8;
  else return 36.1;
}

double getYield(ModelConfig *mc, TString varName){
  double yield=0;
  if(!mc->GetWS()->var(varName)){
    // cerr<<"Variable "<<varName<<" does not exist"<<endl;
    return 0;
  }
  // cout<<mc->GetWS()->var(varName)<<endl;
  vector<pair<TString, double> > list=traceBack(mc->GetWS()->var(varName), mc);
  for(auto item : list){
    cout<<item.first<<" "<<item.second<<endl;
    yield+=item.second/getLuminosity(item.first);
  }
  return yield;
}

void HowManyHiggs(TString inputFileName="HCombCouplingFall2017/workspaces/v6/parameterization/WS-Comb-prodBRFun_80ifb.root"){
  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval) ;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  
  TFile *f=TFile::Open(inputFileName);
  RooWorkspace *w=(RooWorkspace*)f->Get("combWS");
  ModelConfig *mc=(ModelConfig*)w->obj("ModelConfig");
  TIterator *nuisIter=mc->GetNuisanceParameters()->createIterator();
  RooRealVar *nuis=NULL;
  while((nuis=dynamic_cast<RooRealVar*>(nuisIter->Next()))){
    TString nuisName=nuis->GetName();
    if(nuisName.BeginsWith("ATLAS")||nuisName.BeginsWith("TheorySig")) nuis->setVal(0);
  }
  
  TString decay[]={"yy", "ZZ", "WW", "tautau", "bb"};
  double sigma_ggF=4.852E+01+4.863E-01, sigma_VBF=3.779E+00, sigma_ttH=5.065E-01+7.426E-02+1.517E-02+2.875E-03;
  double sigma_ZH=8.824E-01, sigma_WH=1.369E+00, sigma_VH=sigma_WH+sigma_ZH;
  double sigma_tot=sigma_ggF+sigma_VBF+sigma_WH+sigma_ZH+sigma_ttH;
  
  double BR[]={2.270E-03, 2.641E-02, 2.152E-01, 6.256E-02, 5.809E-01};

  double Ntot=sigma_tot*1000, Ntot_ggF=sigma_ggF*1000, Ntot_VBF=sigma_VBF*1000, Ntot_VH=sigma_VH*1000, Ntot_ttH=sigma_ttH*1000;
  cout<<"In total, "<<Ntot<<" Higgs bosons produced every fb-1 at 13 TeV"<<endl;
  for(int idx=0; idx<5; idx++){
    double yield=getYield(mc, "mu_BR_"+decay[idx]);
    double yield_ggF=getYield(mc, "r_ggF_"+decay[idx]);
    double yield_VBF=getYield(mc, "r_VBF_"+decay[idx]);
    double yield_VH=getYield(mc, "r_VH_"+decay[idx]);
    double yield_ttH=getYield(mc, "r_ttH_"+decay[idx]);
    cout<<Form("%s \t %f\t%f \t %f\t%f \t %f\t%f \t %f\t%f \t %f\t%f", decay[idx].Data(), Ntot*BR[idx], yield, Ntot_ggF*BR[idx], yield_ggF, Ntot_VBF*BR[idx], yield_VBF, Ntot_VH*BR[idx], yield_VH, Ntot_ttH*BR[idx], yield_ttH)<<endl;
  }
  f->Close();
}
