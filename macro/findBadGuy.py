#!/usr/bin/python
import argparse
import sys, os, time, commands, getopt, copy, pickle, math
import ROOT

threshold_pull_bad=0.7
threshold_constr_bad=0.5

def extractImpact(fileName, threshold_pull, threshold_constr):
    f=ROOT.TFile.Open(fileName)
    if not f: raise Exception("File "+fileName+" cannot be opened")
    t=f.Get("result")
    POI_hat=0
    POI_up=0
    POI_down=0

    # t.SetBranchAddress(POI+"_hat",POI_hat)
    # t.SetBranchAddress(POI+"_up",POI_up)
    # t.SetBranchAddress(POI+"_down",POI_down)
    for event in t:
        # nuisance=event.nuisance
        # nuis_nom=event.nuis_nom
        # nuis_hat=event.nuis_hat
        # nuis_hi=event.nuis_hi
        # nuis_lo=event.nuis_lo
        # nuis_prefit=event.nuis_prefit
        #print POI_hat, POI_up, POI_down
        NPName=event.nuisance
        cenVal=event.nuis_hat
        upErr=event.nuis_hi
        dnErr=event.nuis_lo
        if math.fabs(cenVal)>threshold_pull or (upErr-dnErr)/2<threshold_constr:
            if math.fabs(cenVal)>threshold_pull_bad or (upErr-dnErr)/2<threshold_constr_bad:
                print "\033[91m %50s \t %.2f \t %.2f \t %.2f \033[0m" %(NPName, cenVal, upErr, dnErr)
            else:
                print "%50s \t %.2f \t %.2f \t %.2f" %(NPName, cenVal, upErr, dnErr)

    f.Close()
    
parser = argparse.ArgumentParser()

parser.add_argument('--inputDirList', type=str, required=True, help='Input list of directories saving ranking results')
# parser.add_argument('--POI', type=str, required=True, help='Name of POI')
parser.add_argument('--threshold_pull', type=float, default=0.5, help="Threshold for the NP pull (default 0.5)")
parser.add_argument('--threshold_constr', type=float, default=0.7, help="Threshold for the NP over-constraint (default 0.7)")


args = parser.parse_args()

fileList={}

print args

for inputDir in args.inputDirList.split(','):
    #print inputDir
    fileList[inputDir]=os.listdir(inputDir)
#print fileList

for inputDir,List in fileList.iteritems():
    print 
    print inputDir
    print
    print "%50s \t %s \t %s \t %s\n" %("NP name", "Pull", "Err+", "Err-")
    for fileName in List:
        if ".root" not in fileName: continue
        if fileName.startswith("unconstr"): continue
        if fileName.startswith("gamma"): continue
        if fileName.startswith("ATLAS_norm"): continue
        extractImpact(inputDir+"/"+fileName, args.threshold_pull, args.threshold_constr)
        # NPName=fileName.split('.')[0]
        # #print NPName
        # f=TFile.Open(inputDir+"/"+fileName)
        # h=f.Get(NPName)
        # cenVal=h.GetBinContent(1)
        # upErr=h.GetBinContent(2)
        # dnErr=h.GetBinContent(3)
        # if math.fabs(cenVal)>threshold_pull: print NPName, cenVal, upErr, dnErr#, fileName
        # elif (upErr+dnErr)/2<threshold_constr: print NPName, cenVal, upErr, dnErr#, fileName
        # f.Close();
