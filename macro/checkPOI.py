from ROOT import *
#pOIFile=open("combination_xs_noBR_splitggZH.xml","r")
#pOIFile=open("hcombcouplingsummer2020/config/v0/combination/template/combination_xs_noBR_splitggZH.xml","r")
#pOIFile=open("hcombcouplingsummer2020/config/v0/combination/template/combination_5XS.xml","r")
pOIFile=open("hcombcouplingsummer2020/config/v0/combination/template/combination_mu.xml","r")

channel=pOIFile.read().split("<Channel Name=\"")
channelName=[]

for iChannel in range(len(channel)):
    if iChannel==0:
        continue
    channelName.append(channel[iChannel].split("\"")[0])

print (channelName)

pOIFile.seek(0)

channelFile=pOIFile.read().split("<File Name=\"")
channelFilePath=[]
for iChannel in range(len(channelFile)):
    if iChannel==0:
        continue
    channelFilePath.append(channelFile[iChannel].split("\"")[0])

pOIFile.seek(0)

channelPOI=pOIFile.read().split("<ModelPOI Name=\"")
channelPOIName=[]

for iChannel in range(len(channelPOI)):
    if iChannel==0:
        continue
    channelPOIList=channelPOI[iChannel].split("\"")[0].replace("[1~1]","")
    channelPOIList=''.join(channelPOIList.split())
    channelPOIName.append(channelPOIList.split(","))

#print channelPOIName

#pOICheckFile=open("hcombcouplingsummer2020/config/v0/combination/template/POI5XS.csv","w")
pOICheckFile=open("hcombcouplingsummer2020/config/v0/combination/template/POI.csv","w")

for iChannel in range(len(channelName)):
    pOICheckFile.write(channelName[iChannel])

    if iChannel==(len(channelName)-1):
        pOICheckFile.write("\n")
    else:
        pOICheckFile.write(",")

for iPOI in range(len(channelPOIName[0])):
    for iChannel in range(len(channelPOIName)):
        pOICheckFile.write(channelPOIName[iChannel][iPOI])

        if iChannel==(len(channelPOIName)-1):
            pOICheckFile.write("\n")
        else:
            pOICheckFile.write(",")

#for iChannel in range(len(channelFilePath)):
#    wsFile=TFile(channelFilePath[iChannel])
#    wsChannel=wsFile.combWS
#    #print channelPOIName[iChannel]
#    for iPOI in range(len(channelPOIName[iChannel])):
#        if "dummy" in channelPOIName[iChannel][iPOI]:
#            continue
#
#        if not wsChannel.var(channelPOIName[iChannel][iPOI]):
#            pOICheckFile.write("%s,%s,doesn't exit\n"%(channelName[iChannel],channelPOIName[iChannel][iPOI]))
#        #pOICheckFile.write("%s\n"%(wsChannel.var(channelPOIName[iChannel][iPOI]).Print()))
